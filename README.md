Sebwite Route-crumbs
====================

[![Build Status](https://img.shields.io/travis/sebwite/route-crumbs.svg?&style=flat-square)](https://travis-ci.org/sebwite/route-crumbs)
[![Scrutinizer coverage](https://img.shields.io/scrutinizer/coverage/g/sebwite/route-crumbs.svg?&style=flat-square)](https://scrutinizer-ci.com/g/sebwite/route-crumbs)
[![Scrutinizer quality](https://img.shields.io/scrutinizer/g/sebwite/route-crumbs.svg?&style=flat-square)](https://scrutinizer-ci.com/g/sebwite/route-crumbs)
[![Source](http://img.shields.io/badge/source-sebwite/route-crumbs-blue.svg?style=flat-square)](https://github.com/sebwite/route-crumbs)
[![License](http://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](https://tldrlegal.com/license/mit-license)

Sebwite Route-crumbs is a package for the Laravel 5 framework.

The package follows the FIG standards PSR-1, PSR-2, and PSR-4 to ensure a high level of interoperability between shared PHP code.

Documentation
-------------
Tbd

Quick Installation
------------------
Begin by installing the package through Composer.

```bash
composer require sebwite/route-crumbs
```

