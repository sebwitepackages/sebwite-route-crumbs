<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\RouteCrumbs\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * This is the class RouteCrumbs.
 *
 * @package        Sebwite\RouteCrumbs
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
class RouteCrumbs extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'route_crumbs';
    }
}
