<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\RouteCrumbs;

use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Sebwite\RouteCrumbs\Contracts\Factory as FactoryContract;

/**
 * This is the RouteCrumbs Factory that it responsible for..
 *
 * @package        Sebwite\RouteCrumbs
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
class Factory implements FactoryContract
{
    protected $breadcrumbs;

    protected $models;

    /** Instantiates the class
     *
     * @param \DaveJamesMiller\Breadcrumbs\Manager $container
     */
    public function __construct(Container $container)
    {
        $this->breadcrumbs = $container->make('breadcrumbs');
    }

    /**
     * get models value
     *
     * @return mixed
     */
    public function getModels()
    {
        return $this->models;
    }

    /**
     * Set the models value
     *
     * @param mixed $models
     * @return Factory
     */
    public function setModels($models)
    {
        $this->models = [ ];
        foreach ($models as $name => $model) {
            if ($model instanceof Model) {
                $this->models[ $name ] = $model;
            }
        }

        return $this;
    }

    public function hasModels()
    {
        return isset($this->models) && is_array($this->models) && count($this->models) > 0;
    }

    public function unsetModels()
    {
        $this->models = [ ];

        return $this;
    }

    public function getModel($name)
    {
        return $this->models[ $name ];
    }

    public function hasModel($name)
    {
        return $this->hasModels() && isset($this->models[ $name ]);
    }


    /**
     * get breadcrumbs value
     *
     * @return \DaveJamesMiller\Breadcrumbs\Manager
     */
    public function getBreadcrumbs()
    {
        return $this->breadcrumbs;
    }

    /**
     * Set the breadcrumbs value
     *
     * @param \DaveJamesMiller\Breadcrumbs\Manager $breadcrumbs
     * @return Factory
     */
    public function setBreadcrumbs($breadcrumbs)
    {
        $this->breadcrumbs = $breadcrumbs;

        return $this;
    }
}
