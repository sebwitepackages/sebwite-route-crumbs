<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\RouteCrumbs;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ScopeInterface;
use Sebwite\RouteCrumbs\Contracts\DefinesParentModel;

/**
 * This is the RouteParentsScope.
 *
 * @package        Ccblearning\Courses
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 */
class ParentColumnsScope implements ScopeInterface
{
    protected function buildParentsQuery(Builder $builder, Model $model)
    {
        if (!$model instanceof DefinesParentModel) {
            return;
        }
        $query   = $builder->getQuery();
        $table   = $model->getTable();
        $primary = $model->getKeyName();

        list($columnName, $parentModelClass) = $model::getParentModel();
        $parentModel   = $this->createModel($parentModelClass);
        $parentTable   = $parentModel->getTable();
        $parentPrimary = $parentModel->getKeyName();
        if (!$parentModel instanceof DefinesParentModel) {
            return;
        }
        list($parentParentTableColumnName) = $parentModel::getParentModel();
        $prefix = app('db')->connection()->getTablePrefix();
        $query->selectSub("SELECT {$prefix}{$parentTable}.{$parentParentTableColumnName} FROM {$prefix}{$parentTable} WHERE {$prefix}{$parentTable}.{$parentPrimary} = {$columnName}", $parentParentTableColumnName);
//        SELECT *,
//  (SELECT sections.chapter_id FROM sections WHERE sections.id = ccb_pages.section_id) AS chapter_id,
//  (SELECT chapters.content_id FROM chapters WHERE chapters.id = chapter_id) AS content_id,
//  (SELECT contents.module_id FROM contents WHERE contents.id = content_id) AS module_id,
//  (SELECT modules.course_id FROM modules WHERE modules.id = module_id) AS course_id
//FROM ccb_pages

        if ($parentModel instanceof DefinesParentModel) {
            $this->buildParentsQuery($builder, $parentModel);
        }
    }

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model   $model
     *
     * @return \Illuminate\Database\Eloquent\Builder|void
     */
    public function apply(Builder $builder, Model $model)
    {
        if (!$model instanceof DefinesParentModel) {
            return;
        }
        $builder->getQuery()->select();
        $this->buildParentsQuery($builder, $model);
    }

    /**
     * Remove the scope from the given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model   $model
     *
     * @return void
     */
    public function remove(Builder $builder, Model $model)
    {
    }

    /**
     * Create a new instance of the model.
     *
     * @param  array $data
     *
     * @return mixed
     */
    public function createModel($class, array $data = [ ])
    {
        $class = '\\' . ltrim($class, '\\');

        return new $class($data);
    }
}
