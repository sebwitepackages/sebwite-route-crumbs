<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\RouteCrumbs\Routing;

use Illuminate\Database\Eloquent\Model;
use Sebwite\RouteCrumbs\Traits\CreatesBreadcrumbRoutes;

/**
 * This is the Router class that will be used by default to enable RouteCrumbs.
 *
 * @package        Sebwite\RouteCrumbs
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 */
class Router extends \Illuminate\Routing\Router
{
    use CreatesBreadcrumbRoutes;
}
