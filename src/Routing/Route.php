<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\RouteCrumbs\Routing;

use Sebwite\RouteCrumbs\Contracts\BreadcrumbsRoute;
use Sebwite\RouteCrumbs\Traits\RouteCrumbsTrait;

/**
 * This is the Route.
 *
 * @package        Sebwite\RouteCrumbs
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 */
class Route extends \Illuminate\Routing\Route implements BreadcrumbsRoute
{
    use RouteCrumbsTrait;
}
