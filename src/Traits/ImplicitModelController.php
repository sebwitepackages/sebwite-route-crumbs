<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\RouteCrumbs\Traits;

/**
 * This is the class ImplicitModelController.
 *
 * @package        Sebwite\RouteCrumbs
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
trait ImplicitModelController
{

    /**
     * Execute an action on the controller.
     *
     * @param  string $method
     * @param  array  $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, $parameters)
    {
        $newParams = [ ];
        $ref       = new \ReflectionMethod($this, $method);
        foreach ($ref->getParameters() as $param) {
            if (!array_key_exists($param->getName(), $parameters)) {
                continue;
            }
            $newParams[ $param->getName() ] = $parameters[ $param->getName() ];
        }

        $parameters = $newParams;

        return call_user_func_array([ $this, $method ], $parameters);
    }
}
