<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\RouteCrumbs\Traits;

use Closure;
use Sebwite\RouteCrumbs\Contracts\DefinesParentModel;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * This is the class CreatesBreadcrumbRoutes.
 *
 * This should be used in a class based on `Illuminate\Routing\Router`.
 *
 * @package        Sebwite\RouteCrumbs
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 * @property \Illuminate\Container\Container $container
 * @mixin \Illuminate\Routing\Router
 */
trait CreatesBreadcrumbRoutes
{
    /**
     * The Route class that will be used when creating new Route objects by the newRoute() method
     *
     * @var string
     */
    protected $routeClass = \Sebwite\RouteCrumbs\Routing\Route::class;

    /**
     * Create a new Route object.
     *
     * @param array|string $methods
     * @param string       $uri
     * @param mixed        $action
     * @return \Sebwite\RouteCrumbs\Routing\Route
     */
    protected function newRoute($methods, $uri, $action)
    {
        $class = $this->routeClass;
        /**
         * @var \Sebwite\RouteCrumbs\Routing\Route $route
         */
        $route = new $class($methods, $uri, $action);
        $route->setContainer($this->container);
        $route->setRouter($this);
        $route->setRouteCrumbs($this->container->make('route_crumbs'));

        return $route;
    }

    /**
     * Returns the Route class that will be used when creating new Route objects by the newRoute() method
     *
     * @return string
     */
    public function getRouteClass()
    {
        return $this->routeClass;
    }

    /**
     * Sets the Route class name that will be used when creating new Route objects by the newRoute() method
     *
     * @param string $routeClass
     * @return self
     */
    public function setRouteClass($routeClass)
    {
        $this->routeClass = $routeClass;

        return $this;
    }
}
