<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\RouteCrumbs\Traits;

use DaveJamesMiller\Breadcrumbs\Generator;
use Sebwite\RouteCrumbs\Contracts\BreadcrumbsRoute;
use Sebwite\Support\Str;

/**
 * This is the class RouteCrumbsRouteTrait.
 *
 * This should be used in a class based on `\Illuminate\Routing\Route`
 *
 * @package        Sebwite\RouteCrumbs
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 * @property \Illuminate\Contracts\Container\Container $container
 * @mixin \Illuminate\Routing\Route
 */
trait RouteCrumbsTrait
{
    /**
     * The RouteCrumbs factory object instance
     * @var \Sebwite\RouteCrumbs\Factory
     */
    protected $routeCrumbs;

    /**
     * Get the URL to the route
     *
     * @return string
     */
    public function getUrl()
    {
        $name = $this->getName();
        $this->bindParameters(request());

        return $this->container->make('url')->route(
            $this->getName(),
            count($this->parameterNames()) > 0 ? $this->parameters() : [ ]
        );
    }

    /**
     * Defines a breadcrumb for this route.
     *
     * @todo Improve working. This does not generate proper breadcrumbs for all situations
     * @param  string                                                     $title The title of the breadcrumb
     * @param string|null|\Sebwite\RouteCrumbs\Contracts\BreadcrumbsRoute $parentRoute Optional parent route. Either a route name or a Route object
     * @return $this
     */
    public function crumb($title, $parentRoute = null)
    {
        if (! $parentRoute instanceof BreadcrumbsRoute && is_string($parentRoute)) {
            $routes = $this->container->make('router')->getRoutes();
            if ($routes->hasNamedRoute($parentRoute)) {
                $parentRoute = $routes->getByName($parentRoute);
            }
        }


        $route       = $this;
        $router      = app('router');
        $routeCrumbs = $this->routeCrumbs;
        $routeCrumbs->getBreadcrumbs()->register($this->getName(), function (Generator $breadcrumbs, $any = null) use ($route, $title, $parentRoute, $router, $routeCrumbs) {



            if (! is_null($parentRoute)) {
                $breadcrumbs->parent(is_string($parentRoute) ? $parentRoute : $parentRoute->getName());
            }


            $url       = '';
            $matches   = [ ];
            $usesModel = preg_match('/\{(.*?)\}/', $title, $matches) > 0;
            if ($usesModel) {
                if (! $routeCrumbs->hasModels()) {
                    $currentRoute = $router->getCurrentRoute();
                    $routeCrumbs->setModels($currentRoute->parameters());
                }


                $segments = explode('.', $matches[ 1 ]);
                if ($routeCrumbs->hasModel($name = array_shift($segments))) {
                /**
                     * @var \Illuminate\Database\Eloquent\Model $value
                     */
                    $value = $routeCrumbs->getModel($name);
                    $key   = $value->getKey();
                    if (method_exists($value, 'getRouteParams')) {
                        $url = route($route->getName(), $value->getRouteParams());
                    } else {
                        $url = route($route->getName(), $key);
                    }
                    foreach ($segments as $segment) {
                        if (isset($value[ $segment ])) {
                            $value = $value[ $segment ];
                        }
                    }

                    $title = str_replace($matches[ 0 ], $value, $title);

                    if ($titleMax = config('route-crumbs.title-max-length')) {
                        $title = Str::limit($title, $titleMax, config('route-crumbs.title-max-end'));
                    }
                }
            } else {
                $url = $route->getUrl();
            }

            $breadcrumbs->push($title, $url);
        });


        return $this;
    }

    /**
     * Returns the RouteCrumbs factory object instance
     *
     * @return \Sebwite\RouteCrumbs\Factory
     */
    public function getRouteCrumbs()
    {
        return $this->routeCrumbs;
    }

    /**
     * Set the RouteCrumbs factory object instance
     *
     * @param \Sebwite\RouteCrumbs\Factory $routeCrumbs
     * @return self
     */
    public function setRouteCrumbs($routeCrumbs)
    {
        $this->routeCrumbs = $routeCrumbs;

        return $this;
    }
}
