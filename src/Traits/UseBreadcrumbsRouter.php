<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\RouteCrumbs\Traits;

/**
 * This is the UseBreadcrumbsRouter trait. To be used in a Http Kernel.
 *
 * @package        Sebwite\RouteCrumbs
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 */
trait UseBreadcrumbsRouter
{
    /**
     * Get the route dispatcher callback.
     *
     * @return \Closure
     */
    protected function dispatchToRouter()
    {
        $this->router = $this->app[ 'router' ];
        foreach ($this->routeMiddleware as $key => $middleware) {
            $this->router->middleware($key, $middleware);
        }

        return parent::dispatchToRouter();
    }
}
