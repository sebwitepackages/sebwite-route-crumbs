<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\RouteCrumbs\Traits;

use Sebwite\Support\Arr;

/**
 * This is the RouteCrumbsModelTrait.
 *
 * @package        Sebwite\RouteCrumbs
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 * @mixin \Ccblearning\Modules\Database\Module
 */
trait RouteCrumbsModelTrait
{
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return $this->getQualifiedKeyName();
    }

    public function getRouteParams(array $except = [ ])
    {

        $params = [ ];

        if (! property_exists($this, 'routeParams')) {
            return $params;
        }

        $routeParams = Arr::except($this->routeParams, $except);

        foreach ($routeParams as $name => $propertyName) {
            $params[ $name ] = $this->{$propertyName};
        }

        return $params;
    }
}
