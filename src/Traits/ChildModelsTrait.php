<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\RouteCrumbs\Traits;

trait ChildModelsTrait
{
    protected static $children = [ ];

    public static function registerChildModel($name, $class = null)
    {
        $class                     = $class ?: static::class;
        static::$children[ $name ] = $class;
    }

    public static function getChildModels()
    {
        return static::$children;
    }
}
