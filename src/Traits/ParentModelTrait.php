<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\RouteCrumbs\Traits;

use Sebwite\RouteCrumbs\Contracts\DefinesChildModels;
use Sebwite\RouteCrumbs\Contracts\DefinesParentModel;
use Sebwite\RouteCrumbs\ParentColumnsScope;

/**
 * This is the RouteParentsTrait.
 *
 * @package        Ccblearning\Courses
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 * @mixin \Ccblearning\Contents\Database\Content
 */
trait ParentModelTrait
{

    public static function bootParentModelTrait()
    {
        static::addGlobalScope(new ParentColumnsScope);
        $instance = new static;
        $class    = get_class($instance);
        $name     = strtolower(last(explode('\\', $class)));

        if ($instance instanceof DefinesParentModel) {
            list($key, $parent) = $instance::getParentModel();
            $parent = new $parent;
            if ($parent instanceof DefinesChildModels) {
                $parent::registerChildModel($name, $class);
            }
        }

    }

    public function parent()
    {
        if (!$this instanceof DefinesParentModel) {
            throw new \LogicException(get_called_class() . ' should implement ' . DefinesParentModel::class);
        }

        return $this->belongsTo(last(static::getParentModel()));
    }
}
