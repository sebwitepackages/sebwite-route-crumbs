<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\RouteCrumbs\Contracts;

interface DefinesChildModels
{

    public static function registerChildModel($name, $class = null);

    public static function getChildModels();
}
