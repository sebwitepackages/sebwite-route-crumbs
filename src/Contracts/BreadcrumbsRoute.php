<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\RouteCrumbs\Contracts;

/**
 * This is the class BreadcrumbsRoute.
 *
 * @package        Sebwite\RouteCrumbs
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
interface BreadcrumbsRoute
{

}
