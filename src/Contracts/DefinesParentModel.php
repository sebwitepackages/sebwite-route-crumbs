<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\RouteCrumbs\Contracts;

/**
 * This is the RouteParentsModel.
 *
 * @package        Ccblearning\Courses
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 */
interface DefinesParentModel
{
    public static function getParentModel();
}
