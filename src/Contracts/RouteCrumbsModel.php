<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\RouteCrumbs\Contracts;

/**
 * This is the RouteCrumbsModel.
 *
 * @package        Sebwite\RouteCrumbs
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 */
interface RouteCrumbsModel
{
    public function getRouteParams(array $except = [ ]);
}
