<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */

namespace Sebwite\RouteCrumbs;

use Sebwite\Support\ServiceProvider;

/**
 * The main service provider
 *
 * @author        Sebwite
 * @copyright     Copyright (c) 2015, Sebwite
 * @package       Sebwite\RouteCrumbs
 */
class RouteCrumbsServiceProvider extends ServiceProvider
{
    protected $dir = __DIR__;

    protected $configFiles = [ 'route-crumbs' ];

    protected $providers = [
        \DaveJamesMiller\Breadcrumbs\ServiceProvider::class
    ];

    protected $singletons = [
        'route_crumbs' => Factory::class
    ];

    protected $aliases = [
        'route_crumbs' => Contracts\Factory::class
    ];

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $app = parent::boot();
    }

    /**
     * {@inheritdoc}
     */
    public function register()
    {
        $app = parent::register();

        if ($app[ 'config' ][ 'route-crumbs.custom-router' ] === false) {
            $this->registerRouter();
        }
    }


    protected function registerRouter()
    {
        $this->app[ 'router' ] = $this->app->share(function ($app) {
            $router = new Routing\Router($app[ 'events' ], $app);
            $router->setRouteClass($app[ 'config' ][ 'route-crumbs.route-class' ]);
            return $router;
        });
    }
}
