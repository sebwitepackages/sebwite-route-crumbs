<!---
title: Overview
author: Sebwite
-->

## Installation

#### Composer

Begin by installing the package through Composer.

```json
{
    "require": {
        "sebwite/route-crumbs": "1.0.*"
    }
}
```

#### Laravel

1. Add the service provider to the `config/app.php` file.

```php
return [
    'providers' => [
        Sebwite\RouteCrumbs\RouteCrumbsServiceProvider::class,    
    ]    
];
```

2. Add the `UseBreadcrumbsRouter` trait to your Http Kernel `app/Http/
```php
class Kernel extends HttpKernel
{
    use UseBreadcrumbsRouter;
}
```


## Usage

#### Basic usage
```php
Route::group(['as' => 'modules.',], function(){
    $route = Route::get('/', [ 'as' => 'list', 'uses' => 'ModulesController@showList' ])->crumb('Module list');
    Route::get('{module}', [ 'as' => 'show', 'uses' => 'ModulesController@show' ])->crumb('View', $route);
});
```

#### With parent routename
```php
Route::group(['as' => 'modules.',], function(){
    $route = Route::get('/', [ 'as' => 'list', 'uses' => 'ModulesController@showList' ])->crumb('Module list');
    Route::get('{module}', [ 'as' => 'show', 'uses' => 'ModulesController@show' ])->crumb('View', 'modules.list');
});
```

#### With model routes
The `$title` parameter of the `crumb` method can make use of model properties like so:
```php
Route::model('module', Modules\Module::class);
Route::group(['as' => 'modules.',], function(){
    $route = Route::get('/', [ 'as' => 'list', 'uses' => 'ModulesController@showList' ])->crumb('Module list');
    Route::get('{module}', [ 'as' => 'show', 'uses' => 'ModulesController@show' ])->crumb('Module: {module.name}', 'modules.list');
});
```
