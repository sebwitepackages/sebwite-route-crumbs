<?php

return [
    /*
     * By default, RouteCrumbs uses its own Router implementation.
     * If you already have your own Router implementation, you can disable
     * The RouteCrumbs Router by setting this to true.
     *
     * Remember to apply the CreatesBreadcrumbRoutes trait to your Router class.
     */
    'custom-router' => false,

    /*
     * If you want to change the class that will be created by CreatesBreadcrumbRoutes trait, do it here.
     *
     * Remember to apply the RouteCrumbsTrait to your Route class.
     */
    'route-class' => Sebwite\RouteCrumbs\Routing\Route::class,

    'title-max-length' => 15,
    'title-max-end' => '...'
];
